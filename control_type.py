from enum import Enum


class ControlTypeId(Enum):
    AppBarControl = 50040
    ButtonControl = 50000
    CalendarControl = 50001
    CheckBoxControl = 50002
    ComboBoxControl = 50003
    CustomControl = 50025
    DataGridControl = 50028
    DataItemControl = 50029
    DocumentControl = 50030
    EditControl = 50004
    GroupControl = 50026
    HeaderControl = 50034
    HeaderItemControl = 50035
    HyperlinkControl = 50005
    ImageControl = 50006
    ListControl = 50008
    ListItemControl = 50007
    MenuBarControl = 50010
    MenuControl = 50009
    MenuItemControl = 50011
    PaneControl = 50033
    ProgressBarControl = 50012
    RadioButtonControl = 50013
    ScrollBarControl = 50014
    SemanticZoomControl = 50039
    SeparatorControl = 50038
    SliderControl = 50015
    SpinnerControl = 50016
    SplitButtonControl = 50031
    StatusBarControl = 50017
    TabControl = 50018
    TabItemControl = 50019
    TableControl = 50036
    TextControl = 50020
    ThumbControl = 50027
    TitleBarControl = 50037
    ToolBarControl = 50021
    ToolTipControl = 50022
    TreeControl = 50023
    TreeItemControl = 50024
    WindowControl = 50032
