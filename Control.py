import re
import locale
import comtypes
from typing import Self, Optional, Tuple, Any, List, Callable, Union
from datetime import datetime

from bounding_rect import BoundingRect
from control_type import ControlTypeId
from controls.constants import (
    DEFAULT_SEARCH_INTERVAL,
    DEFAULT_TIMEOUT_SECONDS,
    DEFAULT_OPERATION_WAIT_TIME,
    DEBUG_SEARCH_TIME,
)
from patterns.legacy_iaccessible_pattern import LegacyIAccessiblePattern
from patterns.pattern_id import PatternId
from patterns.value_pattern import ValuePattern
from properties.clickable_point_property import ClickablePointProperty
from searching.search_properties import SearchProperties
from abc import ABC, abstractmethod
import logging
import time

from stubs.iui_stubs import IUIAutomationElement

ProcessTime = time.perf_counter
ProcessTime()

logger = logging.getLogger(__name__)


class Control(ABC):
    ValidKeys = {
        "ControlType",
        "ClassName",
        "AutomationId",
        "Name",
        "SubName",
        "RegexName",
        "Depth",
        "Compare",
    }

    def __init__(
        self,
        search_from_control: Self = None,
        search_depth: int = 0xFFFFFFFF,
        search_interval: float = DEFAULT_SEARCH_INTERVAL,
        found_index: int = 1,
        element: IUIAutomationElement = None,
        search_properties: Optional[SearchProperties] = None,
    ):
        """
        `Control` wraps IUIAutomationElement.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nn-uiautomationclient-iuiautomationelement
        """
        self._element = element
        self._elementDirectAssign = True if element else False
        self.search_from_control = search_from_control
        if search_properties is not None:
            self.search_depth = search_properties.depth
        self.search_interval = search_interval
        self.found_index = found_index
        self.search_properties = search_properties
        if self.search_properties is not None:
            self.regex_name = (
                re.compile(self.search_properties.regex_name)
                if self.search_properties.regex_name
                else None
            )
        self._supportedPatterns = {}

    def __str__(self) -> str:
        # rect = self.BoundingRectangle
        # self.
        return f"ControlType: {self.control_type_name}    ClassName: {self.class_name}    AutomationId: {self.automation_id}    Rect: {self.bounding_rectangle}    Name: {self.name}    Handle: {self.native_window_handle})"

    @property
    @abstractmethod
    def control_type_name(self) -> str:
        pass

    @staticmethod
    def SetSearchFromControl(self, searchFromControl: "Control") -> None:
        """searchFromControl: `Control` or its subclass"""
        self.search_from_control = searchFromControl

    def SetSearchDepth(self, searchDepth: int) -> None:
        self.search_depth = searchDepth

    def AddSearchProperties(self, **searchProperties) -> None:
        """
        Add search properties using `dict.update`.
        searchProperties: dict, same as searchProperties in `Control.__init__`.
        """
        self.searchProperties.update(searchProperties)
        if "Depth" in searchProperties:
            self.search_depth = searchProperties["Depth"]
        if "RegexName" in searchProperties:
            regName = searchProperties["RegexName"]
            self.regexName = re.compile(regName) if regName else None

    def RemoveSearchProperties(self, **searchProperties) -> None:
        """
        searchProperties: dict, same as searchProperties in `Control.__init__`.
        """
        for key in searchProperties:
            del self.searchProperties[key]
            if key == "RegexName":
                self.regexName = None

    def GetSearchPropertiesStr(self) -> str:
        strs = [
            "{}: {}".format(k, ControlTypeNames[v] if k == "ControlType" else repr(v))
            for k, v in self.search_properties.__dict__.items()
        ]
        return "{" + ", ".join(strs) + "}"

    def GetColorfulSearchPropertiesStr(
        self, keyColor="DarkGreen", valueColor="DarkCyan"
    ) -> str:
        """keyColor, valueColor: str, color name in class ConsoleColor"""
        strs = [
            "<Color={}>{}</Color>: <Color={}>{}</Color>".format(
                keyColor if k in Control.ValidKeys else "DarkYellow",
                k,
                valueColor,
                ControlTypeNames[v] if k == "ControlType" else repr(v),
            )
            for k, v in self.search_properties.__dict__.items()
        ]
        return "{" + ", ".join(strs) + "}"

    # BuildUpdatedCache
    # CachedAcceleratorKey
    # CachedAccessKey
    # CachedAriaProperties
    # CachedAriaRole
    # CachedAutomationId
    # CachedBoundingRectangle
    # CachedClassName
    # CachedControlType
    # CachedControllerFor
    # CachedCulture
    # CachedDescribedBy
    # CachedFlowsTo
    # CachedFrameworkId
    # CachedHasKeyboardFocus
    # CachedHelpText
    # CachedIsContentElement
    # CachedIsControlElement
    # CachedIsDataValidForForm
    # CachedIsEnabled
    # CachedIsKeyboardFocusable
    # CachedIsOffscreen
    # CachedIsPassword
    # CachedIsRequiredForForm
    # CachedItemStatus
    # CachedItemType
    # CachedLabeledBy
    # CachedLocalizedControlType
    # CachedName
    # CachedNativeWindowHandle
    # CachedOrientation
    # CachedProcessId
    # CachedProviderDescription

    @property
    def accelerator_key(self) -> str:
        """
        Property AcceleratorKey.
        Call IUIAutomationElement::get_CurrentAcceleratorKey.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentacceleratorkey
        """
        return self.Element.CurrentAcceleratorKey

    @property
    def access_key(self) -> str:
        """
        Property AccessKey.
        Call IUIAutomationElement::get_CurrentAccessKey.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentaccesskey
        """
        return self.Element.CurrentAccessKey

    @property
    def aria_properties(self) -> str:
        """
        Property AriaProperties.
        Call IUIAutomationElement::get_CurrentAriaProperties.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentariaproperties
        """
        return self.Element.CurrentAriaProperties

    @property
    def aria_role(self) -> str:
        """
        Property AriaRole.
        Call IUIAutomationElement::get_CurrentAriaRole.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentariarole
        """
        return self.Element.CurrentAriaRole

    @property
    def automation_id(self) -> str:
        """
        Property AutomationId.
        Call IUIAutomationElement::get_CurrentAutomationId.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentautomationid
        """
        return self.Element.CurrentAutomationId

    @property
    def bounding_rectangle(self) -> BoundingRect:
        """
        Property BoundingRectangle.
        Call IUIAutomationElement::get_CurrentBoundingRectangle.
        Return `Rect`.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentboundingrectangle

        self.Element.CurrentBoundingRectangle returns an instance of: https://github.com/python/cpython/blob/main/Lib/ctypes/wintypes.py
        rect = control.BoundingRectangle
        print(rect.left, rect.top, rect.right, rect.bottom, rect.width(), rect.height(), rect.xcenter(), rect.ycenter())
        """

        rect = self.Element.CurrentBoundingRectangle
        return BoundingRect(rect.left, rect.top, rect.right, rect.bottom)

    @property
    def class_name(self) -> str:
        """
        Property ClassName.
        Call IUIAutomationElement::get_CurrentClassName.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentclassname
        """
        return self.Element.CurrentClassName

    @property
    def control_type(self) -> ControlTypeId:
        """
        Property ControlType.
        Return int, a value in class `ControlType`.
        Call IUIAutomationElement::get_CurrentControlType.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentcontroltype
        """
        return ControlTypeId(self.Element.CurrentControlType)

    # @property
    # def ControllerFor(self):
    # return self.Element.CurrentControllerFor

    @property
    def culture(self) -> str | str:
        """
        Property Culture.
        Call IUIAutomationElement::get_CurrentCulture.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentculture
        """
        try:
            return locale.windows_locale[self.Element.CurrentCulture]
        except KeyError:
            return str(self.Element.CurrentCulture)

    # @property
    # def DescribedBy(self):
    # return self.Element.CurrentDescribedBy

    # @property
    # def FlowsTo(self):
    # return self.Element.CurrentFlowsTo

    @property
    def framework_id(self) -> str:
        """
        Property FrameworkId.
        Call IUIAutomationElement::get_CurrentFrameworkId.
        Return str, such as Win32, WPF...
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentframeworkid
        """
        return self.Element.CurrentFrameworkId

    @property
    def HasKeyboardFocus(self) -> bool:
        """
        Property HasKeyboardFocus.
        Call IUIAutomationElement::get_CurrentHasKeyboardFocus.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currenthaskeyboardfocus
        """
        return bool(self.Element.CurrentHasKeyboardFocus)

    @property
    def HelpText(self) -> str:
        """
        Property HelpText.
        Call IUIAutomationElement::get_CurrentHelpText.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currenthelptext
        """
        return self.Element.CurrentHelpText

    @property
    def IsContentElement(self) -> bool:
        """
        Property IsContentElement.
        Call IUIAutomationElement::get_CurrentIsContentElement.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentiscontentelement
        """
        return bool(self.Element.CurrentIsContentElement)

    @property
    def IsControlElement(self) -> bool:
        """
        Property IsControlElement.
        Call IUIAutomationElement::get_CurrentIsControlElement.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentiscontrolelement
        """
        return bool(self.Element.CurrentIsControlElement)

    @property
    def IsDataValidForForm(self) -> bool:
        """
        Property IsDataValidForForm.
        Call IUIAutomationElement::get_CurrentIsDataValidForForm.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentisdatavalidforform
        """
        return bool(self.Element.CurrentIsDataValidForForm)

    @property
    def IsEnabled(self) -> bool:
        """
        Property IsEnabled.
        Call IUIAutomationElement::get_CurrentIsEnabled.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentisenabled
        """
        return self.Element.CurrentIsEnabled

    @property
    def IsKeyboardFocusable(self) -> bool:
        """
        Property IsKeyboardFocusable.
        Call IUIAutomationElement::get_CurrentIsKeyboardFocusable.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentiskeyboardfocusable
        """
        return self.Element.CurrentIsKeyboardFocusable

    @property
    def IsOffscreen(self) -> bool:
        """
        Property IsOffscreen.
        Call IUIAutomationElement::get_CurrentIsOffscreen.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentisoffscreen
        """
        return self.Element.CurrentIsOffscreen

    @property
    def is_password(self) -> bool:
        """
        Property IsPassword.
        Call IUIAutomationElement::get_CurrentIsPassword.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentispassword
        """
        is_password = bool(self.Element.CurrentIsPassword)
        return is_password

    @property
    def IsRequiredForForm(self) -> bool:
        """
        Property IsRequiredForForm.
        Call IUIAutomationElement::get_CurrentIsRequiredForForm.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentisrequiredforform
        """
        return self.Element.CurrentIsRequiredForForm

    @property
    def ItemStatus(self) -> str:
        """
        Property ItemStatus.
        Call IUIAutomationElement::get_CurrentItemStatus.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentitemstatus
        """
        return self.Element.CurrentItemStatus

    @property
    def ItemType(self) -> str:
        """
        Property ItemType.
        Call IUIAutomationElement::get_CurrentItemType.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentitemtype
        """
        return self.Element.CurrentItemType

    # @property
    # def LabeledBy(self):
    # return self.Element.CurrentLabeledBy

    @property
    def LocalizedControlType(self) -> str:
        """
        Property LocalizedControlType.
        Call IUIAutomationElement::get_CurrentLocalizedControlType.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentlocalizedcontroltype
        """
        return self.Element.CurrentLocalizedControlType

    @property
    def name(self) -> str:
        """
        Property Name.
        Call IUIAutomationElement::get_CurrentName.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentname
        """
        return self.Element.CurrentName or ""  # CurrentName may be None

    @property
    def native_window_handle(self) -> int:
        """
        Property NativeWindowHandle.
        Call IUIAutomationElement::get_CurrentNativeWindowHandle.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentnativewindowhandle
        """
        try:
            handle = self.Element.CurrentNativeWindowHandle
        except comtypes.COMError as ex:
            return 0
        return 0 if handle is None else handle

    @property
    def Orientation(self) -> int:
        """
        Property Orientation.
        Return int, a value in class `OrientationType`.
        Call IUIAutomationElement::get_CurrentOrientation.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentorientation
        """
        return self.Element.CurrentOrientation

    @property
    def ProcessId(self) -> int:
        """
        Property ProcessId.
        Call IUIAutomationElement::get_CurrentProcessId.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentprocessid
        """
        return self.Element.CurrentProcessId

    @property
    def ProviderDescription(self) -> str:
        """
        Property ProviderDescription.
        Call IUIAutomationElement::get_CurrentProviderDescription.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentproviderdescription
        """
        return self.Element.CurrentProviderDescription

    @property
    def value_pattern(self) -> Optional[ValuePattern]:
        return self.get_pattern(PatternId.ValuePattern)

    # FindAll
    # FindAllBuildCache
    # FindFirst
    # FindFirstBuildCache
    # GetCachedChildren
    # GetCachedParent
    # GetCachedPattern
    # GetCachedPatternAs
    # GetCachedPropertyValue
    # GetCachedPropertyValueEx

    def get_clickable_point(self) -> ClickablePointProperty:
        """
        Call IUIAutomationElement::GetClickablePoint.
        Return Tuple[int, int, bool], three items tuple (x, y, gotClickable), such as (20, 10, True)
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-getclickablepoint
        """
        point, gotClickable = self.Element.GetClickablePoint()
        return ClickablePointProperty(point.x, point.y, bool(gotClickable))

    def get_pattern(
        self, pattern_id: PatternId
    ) -> Optional[Union[ValuePattern, LegacyIAccessiblePattern]]:
        """
        Call IUIAutomationElement::GetCurrentPattern.
        Get a new pattern by pattern id if it supports the pattern, and None if it does not
        patternId: int, a value in class `PatternId`.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-getcurrentpattern
        """
        from dependencies import get_pattern_factory

        try:
            pattern = self.Element.GetCurrentPattern(pattern_id.value)
            if pattern is None:
                return None
            if pattern:
                sub_pattern = get_pattern_factory().create_pattern(pattern_id, pattern)
                # subPattern = CreatePattern(patternId, pattern)
                self._supportedPatterns[pattern_id] = sub_pattern
                return sub_pattern
        except comtypes.COMError as ex:
            pass

    def GetPatternAs(self, patternId: int, riid):
        """
        Call IUIAutomationElement::GetCurrentPatternAs.
        Get a new pattern by pattern id if it supports the pattern, todo.
        patternId: int, a value in class `PatternId`.
        riid: GUID.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-getcurrentpatternas
        """
        return self.Element.GetCurrentPatternAs(patternId, riid)

    def GetPropertyValue(self, propertyId: int) -> Any:
        """
        Call IUIAutomationElement::GetCurrentPropertyValue.
        propertyId: int, a value in class `PropertyId`.
        Return Any, corresponding type according to propertyId.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-getcurrentpropertyvalue
        """
        return self.Element.GetCurrentPropertyValue(propertyId)

    def GetPropertyValueEx(self, propertyId: int, ignoreDefaultValue: int) -> Any:
        """
        Call IUIAutomationElement::GetCurrentPropertyValueEx.
        propertyId: int, a value in class `PropertyId`.
        ignoreDefaultValue: int, 0 or 1.
        Return Any, corresponding type according to propertyId.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-getcurrentpropertyvalueex
        """
        return self.Element.GetCurrentPropertyValueEx(propertyId, ignoreDefaultValue)

    def GetRuntimeId(self) -> List[int]:
        """
        Call IUIAutomationElement::GetRuntimeId.
        Return List[int], a list of int.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-getruntimeid
        """
        return self.Element.GetRuntimeId()

    # QueryInterface
    # Release

    def SetFocus(self) -> bool:
        """
        Call IUIAutomationElement::SetFocus.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-setfocus
        """
        try:
            return self.Element.SetFocus() == S_OK
        except comtypes.COMError as ex:
            return False

    @property
    def Element(self) -> IUIAutomationElement:
        """
        Property Element.
        Return `ctypes.POINTER(IUIAutomationElement)`.
        """
        if not self._element:
            self.Refind(
                maxSearchSeconds=DEFAULT_TIMEOUT_SECONDS,
                searchIntervalSeconds=self.search_interval,
            )
        return self._element

    @Element.setter
    def Element(self, value):
        self._element = value

    def GetCachedPattern(self, patternId: int, cache: bool):
        """
        Get a pattern by patternId.
        patternId: int, a value in class `PatternId`.
        Return a pattern if it supports the pattern else None.
        cache: bool, if True, store the pattern for later use, if False, get a new pattern by `self.GetPattern`.
        """
        if cache:
            pattern = self._supportedPatterns.get(patternId, None)
            if pattern:
                return pattern
            else:
                pattern = self.GetPattern(patternId)
                if pattern:
                    self._supportedPatterns[patternId] = pattern
                    return pattern
        else:
            pattern = self.GetPattern(patternId)
            if pattern:
                self._supportedPatterns[patternId] = pattern
                return pattern

    # def GetLegacyIAccessiblePattern(self) -> LegacyIAccessiblePattern:
    #     """
    #     Return `LegacyIAccessiblePattern` if it supports the pattern else None.
    #     """
    #     return self.GetPattern(PatternId.LegacyIAccessiblePattern)

    def GetAncestorControl(
        self, condition: Callable[["Control", int], bool]
    ) -> "Control":
        """
        Get an ancestor control that matches the condition.
        condition: Callable[[Control, int], bool], function(control: Control, depth: int) -> bool,
                   depth starts with -1 and decreses when search goes up.
        Return `Control` subclass or None.
        """
        ancestor = self
        depth = 0
        while True:
            ancestor = ancestor.GetParentControl()
            depth -= 1
            if ancestor:
                if condition(ancestor, depth):
                    return ancestor
            else:
                break

    def GetParentControl(self) -> "Control":
        """
        Return `Control` subclass or None.
        """
        ele = _AutomationClient.instance().ViewWalker.GetParentElement(self.Element)
        return Control.CreateControlFromElement(ele)

    def get_first_child_control(self) -> "Control":
        from dependencies import (
            get_automation_client,
            get_control_factory,
            get_view_walker,
        )

        """
        Return `Control` subclass or None.
        """
        raw_element = get_view_walker().get_first_child_element(self.Element)
        return get_control_factory().create_control_from_element(raw_element)

    def GetLastChildControl(self) -> "Control":
        """
        Return `Control` subclass or None.
        """
        ele = _AutomationClient.instance().ViewWalker.GetLastChildElement(self.Element)
        return Control.CreateControlFromElement(ele)

    def get_next_sibling_control(self) -> "Control":
        """
        Return `Control` subclass or None.
        """
        from dependencies import get_view_walker, get_control_factory

        ele = get_view_walker().get_next_sibling_element(self.Element)
        return get_control_factory().create_control_from_element(ele)

    def get_previous_sibling_control(self) -> "Control":
        """
        Return `Control` subclass or None.
        """
        from dependencies import get_view_walker, get_control_factory

        ele = get_view_walker().get_previous_sibling_element(self.Element)
        return get_control_factory().create_control_from_element(ele)

    def get_sibling_control(
        self, condition: Callable[[Self], bool], forward: bool = True
    ) -> Self:
        """
        Get a sibling control that matches the condition.
        forward: bool, if True, only search next siblings, if False, search pervious siblings first, then search next siblings.
        condition: Callable[[Control], bool], function(control: Control) -> bool.
        Return `Control` subclass or None.
        """
        if not forward:
            prev = self
            while True:
                prev = prev.get_previous_sibling_control()
                if prev:
                    if condition(prev):
                        return prev
                else:
                    break
        next_ = self
        while True:
            next_ = next_.get_next_sibling_control()
            if next_:
                if condition(next_):
                    return next_
            else:
                break

    def GetChildren(self) -> List["Control"]:
        """
        Return List[Control], a list of `Control` subclasses.
        """
        children = []
        child = self.get_first_child_control()
        while child:
            children.append(child)
            child = child.get_next_sibling_control()
        return children

    def _CompareFunction(self, control: "Control", depth: int) -> bool:
        """
        Define how to search.
        control: `Control` or its subclass.
        depth: int, tree depth from searchFromControl.
        Return bool.
        """
        compareFunc = None
        for key, value in self.searchProperties.items():
            if "ControlType" == key:
                if value != control.ControlType:
                    return False
            elif "ClassName" == key:
                if value != control.ClassName:
                    return False
            elif "AutomationId" == key:
                if value != control.AutomationId:
                    return False
            elif "Depth" == key:
                if value != depth:
                    return False
            elif "Name" == key:
                if value != control.Name:
                    return False
            elif "SubName" == key:
                if value not in control.Name:
                    return False
            elif "RegexName" == key:
                if not self.regexName.match(control.Name):
                    return False
            elif "Compare" == key:
                compareFunc = value
        # use Compare at last
        if compareFunc and not compareFunc(control, depth):
            return False
        return True

    def Exists(
        self,
        maxSearchSeconds: float = 5,
        searchIntervalSeconds: float = DEFAULT_SEARCH_INTERVAL,
        printIfNotExist: bool = False,
    ) -> bool:
        from dependencies import get_automation_client

        """
        maxSearchSeconds: float
        searchIntervalSeconds: float
        Find control every searchIntervalSeconds seconds in maxSearchSeconds seconds.
        Return bool, True if find
        """
        if self._element and self._elementDirectAssign:
            # if element is directly assigned, not by searching, just check whether self._element is valid
            # but I can't find an API in UIAutomation that can directly check
            rootElement = GetRootControl().Element
            if self._element == rootElement:
                return True
            else:
                parentElement = (
                    _AutomationClient.instance().ViewWalker.GetParentElement(
                        self._element
                    )
                )
                if parentElement:
                    return True
                else:
                    return False
        # find the element
        if len(self.search_properties) == 0:
            raise LookupError("control's searchProperties must not be empty!")
        self._element = None
        startTime = ProcessTime()
        # Use same timeout(s) parameters for resolve all parents
        prev = self.search_from_control
        if (
            prev
            and not prev._element
            and not prev.Exists(maxSearchSeconds, searchIntervalSeconds)
        ):
            if printIfNotExist or DEBUG_EXIST_DISAPPEAR:
                Logger.ColorfullyLog(
                    self.GetColorfulSearchPropertiesStr()
                    + "<Color=Red> does not exist.</Color>"
                )
            return False
        startTime2 = ProcessTime()
        if DEBUG_SEARCH_TIME:
            startDateTime = datetime.datetime.now()
        while True:
            control = get_automation_client().find_control(
                self.search_from_control,
                self._CompareFunction,
                self.search_depth,
                False,
                self.found_index,
            )
            # control = FindControl(
            #     self.search_from_control,
            #     self._CompareFunction,
            #     self.search_depth,
            #     False,
            #     self.found_index,
            # )
            if control:
                self._element = control.Element
                control._element = 0  # control will be destroyed, but the element needs to be stroed in self._element
                if DEBUG_SEARCH_TIME:
                    Logger.ColorfullyLog(
                        "{} TraverseControls: <Color=Cyan>{}</Color>, SearchTime: <Color=Cyan>{:.3f}</Color>s[{} - {}]".format(
                            self.GetColorfulSearchPropertiesStr(),
                            control.traverseCount,
                            ProcessTime() - startTime2,
                            startDateTime.time(),
                            datetime.datetime.now().time(),
                        )
                    )
                return True
            else:
                remain = startTime + maxSearchSeconds - ProcessTime()
                if remain > 0:
                    time.sleep(min(remain, searchIntervalSeconds))
                else:
                    logger.info("Does not exist")
                    return False

    def Disappears(
        self,
        maxSearchSeconds: float = 5,
        searchIntervalSeconds: float = DEFAULT_SEARCH_INTERVAL,
        printIfNotDisappear: bool = False,
    ) -> bool:
        """
        maxSearchSeconds: float
        searchIntervalSeconds: float
        Check if control disappears every searchIntervalSeconds seconds in maxSearchSeconds seconds.
        Return bool, True if control disappears.
        """
        global DEBUG_EXIST_DISAPPEAR
        start = ProcessTime()
        while True:
            temp = DEBUG_EXIST_DISAPPEAR
            DEBUG_EXIST_DISAPPEAR = False  # do not print for Exists
            if not self.Exists(0, 0, False):
                DEBUG_EXIST_DISAPPEAR = temp
                return True
            DEBUG_EXIST_DISAPPEAR = temp
            remain = start + maxSearchSeconds - ProcessTime()
            if remain > 0:
                time.sleep(min(remain, searchIntervalSeconds))
            else:
                if printIfNotDisappear or DEBUG_EXIST_DISAPPEAR:
                    Logger.ColorfullyLog(
                        self.GetColorfulSearchPropertiesStr()
                        + "<Color=Red> does not disappear.</Color>"
                    )
                return False

    def Refind(
        self,
        maxSearchSeconds: float = DEFAULT_TIMEOUT_SECONDS,
        searchIntervalSeconds: float = DEFAULT_SEARCH_INTERVAL,
        raiseException: bool = True,
    ) -> bool:
        """
        Refind the control every searchIntervalSeconds seconds in maxSearchSeconds seconds.
        maxSearchSeconds: float.
        searchIntervalSeconds: float.
        raiseException: bool, if True, raise a LookupError if timeout.
        Return bool, True if find.
        """
        if not self.Exists(
            maxSearchSeconds,
            searchIntervalSeconds,
            False if raiseException else DEBUG_EXIST_DISAPPEAR,
        ):
            if raiseException:
                logger.debug(
                    "<Color=Red>Find Control Timeout({}s): </Color>{}".format(
                        maxSearchSeconds, self.GetColorfulSearchPropertiesStr()
                    )
                )
                raise LookupError(
                    "Find Control Timeout({}s): {}".format(
                        maxSearchSeconds, self.GetSearchPropertiesStr()
                    )
                )
            else:
                return False
        return True

    def GetPosition(self, ratioX: float = 0.5, ratioY: float = 0.5) -> Tuple[int, int]:
        """
        Gets the position of the center of the control.
        ratioX: float.
        ratioY: float.
        Return Tuple[int, int], two ints tuple (x, y), the cursor positon relative to screen(0, 0)
        """
        rect = self.BoundingRectangle
        if rect.width() == 0 or rect.height() == 0:
            Logger.ColorfullyLog(
                "<Color=Yellow>Can not move cursor</Color>. {}'s BoundingRectangle is {}. SearchProperties: {}".format(
                    self.ControlTypeName, rect, self.GetColorfulSearchPropertiesStr()
                )
            )
            return
        x = rect.left + int(rect.width() * ratioX)
        y = rect.top + int(rect.height() * ratioY)
        return x, y

    def MoveCursorToInnerPos(
        self,
        x: int = None,
        y: int = None,
        ratioX: float = 0.5,
        ratioY: float = 0.5,
        simulateMove: bool = True,
    ) -> Tuple[int, int]:
        """
        Move cursor to control's internal position, default to center.
        x: int, if < 0, move to self.BoundingRectangle.right + x, if not None, ignore ratioX.
        y: int, if < 0, move to self.BoundingRectangle.bottom + y, if not None, ignore ratioY.
        ratioX: float.
        ratioY: float.
        simulateMove: bool.
        Return Tuple[int, int], two ints tuple (x, y), the cursor positon relative to screen(0, 0)
            after moving or None if control's width or height is 0.
        """
        rect = self.BoundingRectangle
        if rect.width() == 0 or rect.height() == 0:
            Logger.ColorfullyLog(
                "<Color=Yellow>Can not move cursor</Color>. {}'s BoundingRectangle is {}. SearchProperties: {}".format(
                    self.ControlTypeName, rect, self.GetColorfulSearchPropertiesStr()
                )
            )
            return
        if x is None:
            x = rect.left + int(rect.width() * ratioX)
        else:
            x = (rect.left if x >= 0 else rect.right) + x
        if y is None:
            y = rect.top + int(rect.height() * ratioY)
        else:
            y = (rect.top if y >= 0 else rect.bottom) + y
        if simulateMove and MAX_MOVE_SECOND > 0:
            MoveTo(x, y, waitTime=0)
        else:
            SetCursorPos(x, y)
        return x, y

    def MoveCursorToMyCenter(self, simulateMove: bool = True) -> Tuple[int, int]:
        """
        Move cursor to control's center.
        Return Tuple[int, int], two ints tuple (x, y), the cursor positon relative to screen(0, 0) after moving.
        """
        return self.MoveCursorToInnerPos(simulateMove=simulateMove)

    def Click(
        self,
        x: int = None,
        y: int = None,
        ratioX: float = 0.5,
        ratioY: float = 0.5,
        simulateMove: bool = True,
        waitTime: float = DEFAULT_OPERATION_WAIT_TIME,
    ) -> None:
        """
        x: int, if < 0, click self.BoundingRectangle.right + x, if not None, ignore ratioX.
        y: int, if < 0, click self.BoundingRectangle.bottom + y, if not None, ignore ratioY.
        ratioX: float.
        ratioY: float.
        simulateMove: bool, if True, first move cursor to control smoothly.
        waitTime: float.

        Click(), Click(ratioX=0.5, ratioY=0.5): click center.
        Click(10, 10): click left+10, top+10.
        Click(-10, -10): click right-10, bottom-10.
        """
        point = self.MoveCursorToInnerPos(x, y, ratioX, ratioY, simulateMove)
        if point:
            Click(point[0], point[1], waitTime)

    def MiddleClick(
        self,
        x: int = None,
        y: int = None,
        ratioX: float = 0.5,
        ratioY: float = 0.5,
        simulateMove: bool = True,
        waitTime: float = DEFAULT_OPERATION_WAIT_TIME,
    ) -> None:
        """
        x: int, if < 0, middle click self.BoundingRectangle.right + x, if not None, ignore ratioX.
        y: int, if < 0, middle click self.BoundingRectangle.bottom + y, if not None, ignore ratioY.
        ratioX: float.
        ratioY: float.
        simulateMove: bool, if True, first move cursor to control smoothly.
        waitTime: float.

        MiddleClick(), MiddleClick(ratioX=0.5, ratioY=0.5): middle click center.
        MiddleClick(10, 10): middle click left+10, top+10.
        MiddleClick(-10, -10): middle click right-10, bottom-10.
        """
        point = self.MoveCursorToInnerPos(x, y, ratioX, ratioY, simulateMove)
        if point:
            MiddleClick(point[0], point[1], waitTime)

    def RightClick(
        self,
        x: int = None,
        y: int = None,
        ratioX: float = 0.5,
        ratioY: float = 0.5,
        simulateMove: bool = True,
        waitTime: float = DEFAULT_OPERATION_WAIT_TIME,
    ) -> None:
        """
        x: int, if < 0, right click self.BoundingRectangle.right + x, if not None, ignore ratioX.
        y: int, if < 0, right click self.BoundingRectangle.bottom + y, if not None, ignore ratioY.
        ratioX: float.
        ratioY: float.
        simulateMove: bool, if True, first move cursor to control smoothly.
        waitTime: float.

        RightClick(), RightClick(ratioX=0.5, ratioY=0.5): right click center.
        RightClick(10, 10): right click left+10, top+10.
        RightClick(-10, -10): right click right-10, bottom-10.
        """
        point = self.MoveCursorToInnerPos(x, y, ratioX, ratioY, simulateMove)
        if point:
            RightClick(point[0], point[1], waitTime)

    def DoubleClick(
        self,
        x: int = None,
        y: int = None,
        ratioX: float = 0.5,
        ratioY: float = 0.5,
        simulateMove: bool = True,
        waitTime: float = DEFAULT_OPERATION_WAIT_TIME,
    ) -> None:
        """
        x: int, if < 0, right click self.BoundingRectangle.right + x, if not None, ignore ratioX.
        y: int, if < 0, right click self.BoundingRectangle.bottom + y, if not None, ignore ratioY.
        ratioX: float.
        ratioY: float.
        simulateMove: bool, if True, first move cursor to control smoothly.
        waitTime: float.

        DoubleClick(), DoubleClick(ratioX=0.5, ratioY=0.5): double click center.
        DoubleClick(10, 10): double click left+10, top+10.
        DoubleClick(-10, -10): double click right-10, bottom-10.
        """
        x, y = self.MoveCursorToInnerPos(x, y, ratioX, ratioY, simulateMove)
        Click(x, y, GetDoubleClickTime() * 1.0 / 2000)
        Click(x, y, waitTime)

    def DragDrop(
        self,
        x1: int,
        y1: int,
        x2: int,
        y2: int,
        moveSpeed: float = 1,
        waitTime: float = DEFAULT_OPERATION_WAIT_TIME,
    ) -> None:
        rect = self.BoundingRectangle
        if rect.width() == 0 or rect.height() == 0:
            Logger.ColorfullyLog(
                "<Color=Yellow>Can not move cursor</Color>. {}'s BoundingRectangle is {}. SearchProperties: {}".format(
                    self.ControlTypeName, rect, self.GetColorfulSearchPropertiesStr()
                )
            )
            return
        x1 = (rect.left if x1 >= 0 else rect.right) + x1
        y1 = (rect.top if y1 >= 0 else rect.bottom) + y1
        x2 = (rect.left if x2 >= 0 else rect.right) + x2
        y2 = (rect.top if y2 >= 0 else rect.bottom) + y2
        DragDrop(x1, y1, x2, y2, moveSpeed, waitTime)

    def RightDragDrop(
        self,
        x1: int,
        y1: int,
        x2: int,
        y2: int,
        moveSpeed: float = 1,
        waitTime: float = DEFAULT_OPERATION_WAIT_TIME,
    ) -> None:
        rect = self.BoundingRectangle
        if rect.width() == 0 or rect.height() == 0:
            Logger.ColorfullyLog(
                "<Color=Yellow>Can not move cursor</Color>. {}'s BoundingRectangle is {}. SearchProperties: {}".format(
                    self.ControlTypeName, rect, self.GetColorfulSearchPropertiesStr()
                )
            )
            return
        x1 = (rect.left if x1 >= 0 else rect.right) + x1
        y1 = (rect.top if y1 >= 0 else rect.bottom) + y1
        x2 = (rect.left if x2 >= 0 else rect.right) + x2
        y2 = (rect.top if y2 >= 0 else rect.bottom) + y2
        RightDragDrop(x1, y1, x2, y2, moveSpeed, waitTime)

    def WheelDown(
        self,
        x: int = None,
        y: int = None,
        ratioX: float = 0.5,
        ratioY: float = 0.5,
        wheelTimes: int = 1,
        interval: float = 0.05,
        waitTime: float = DEFAULT_OPERATION_WAIT_TIME,
    ) -> None:
        """
        Make control have focus first, move cursor to the specified position and mouse wheel down.
        x: int, if < 0, move x cursor to self.BoundingRectangle.right + x, if not None, ignore ratioX.
        y: int, if < 0, move y cursor to self.BoundingRectangle.bottom + y, if not None, ignore ratioY.
        ratioX: float.
        ratioY: float.
        wheelTimes: int.
        interval: float.
        waitTime: float.
        """
        cursorX, cursorY = GetCursorPos()
        self.SetFocus()
        self.MoveCursorToInnerPos(x, y, ratioX, ratioY, simulateMove=False)
        WheelDown(wheelTimes, interval, waitTime)
        SetCursorPos(cursorX, cursorY)

    def WheelUp(
        self,
        x: int = None,
        y: int = None,
        ratioX: float = 0.5,
        ratioY: float = 0.5,
        wheelTimes: int = 1,
        interval: float = 0.05,
        waitTime: float = DEFAULT_OPERATION_WAIT_TIME,
    ) -> None:
        """
        Make control have focus first, move cursor to the specified position and mouse wheel up.
        x: int, if < 0, move x cursor to self.BoundingRectangle.right + x, if not None, ignore ratioX.
        y: int, if < 0, move y cursor to self.BoundingRectangle.bottom + y, if not None, ignore ratioY.
        ratioX: float.
        ratioY: float.
        wheelTimes: int.
        interval: float.
        waitTime: float.
        """
        cursorX, cursorY = GetCursorPos()
        self.SetFocus()
        self.MoveCursorToInnerPos(x, y, ratioX, ratioY, simulateMove=False)
        WheelUp(wheelTimes, interval, waitTime)
        SetCursorPos(cursorX, cursorY)

    def ShowWindow(
        self, cmdShow: int, waitTime: float = DEFAULT_OPERATION_WAIT_TIME
    ) -> bool:
        """
        Get a native handle from self or ancestors until valid and call native `ShowWindow` with cmdShow.
        cmdShow: int, a value in in class `SW`.
        waitTime: float.
        Return bool, True if succeed otherwise False.
        """
        handle = self.NativeWindowHandle
        if not handle:
            control = self
            while not handle:
                control = control.GetParentControl()
                handle = control.NativeWindowHandle
        if handle:
            ret = ShowWindow(handle, cmdShow)
            time.sleep(waitTime)
            return ret

    def Show(self, waitTime: float = DEFAULT_OPERATION_WAIT_TIME) -> bool:
        """
        Call native `ShowWindow(SW.Show)`.
        Return bool, True if succeed otherwise False.
        """
        return self.ShowWindow(SW.Show, waitTime)

    def Hide(self, waitTime: float = DEFAULT_OPERATION_WAIT_TIME) -> bool:
        """
        Call native `ShowWindow(SW.Hide)`.
        waitTime: float
        Return bool, True if succeed otherwise False.
        """
        return self.ShowWindow(SW.Hide, waitTime)

    def MoveWindow(
        self, x: int, y: int, width: int, height: int, repaint: bool = True
    ) -> bool:
        """
        Call native MoveWindow if control has a valid native handle.
        x: int.
        y: int.
        width: int.
        height: int.
        repaint: bool.
        Return bool, True if succeed otherwise False.
        """
        handle = self.NativeWindowHandle
        if handle:
            return MoveWindow(handle, x, y, width, height, int(repaint))
        return False

    def GetWindowText(self) -> str:
        """
        Call native GetWindowText if control has a valid native handle.
        """
        handle = self.NativeWindowHandle
        if handle:
            return GetWindowText(handle)

    def SetWindowText(self, text: str) -> bool:
        """
        Call native SetWindowText if control has a valid native handle.
        """
        handle = self.NativeWindowHandle
        if handle:
            return SetWindowText(handle, text)
        return False

    def SendKey(self, key: int, waitTime: float = DEFAULT_OPERATION_WAIT_TIME) -> None:
        """
        Make control have focus first and type a key.
        `self.SetFocus` may not work for some controls, you may need to click it to make it have focus.
        key: int, a key code value in class Keys.
        waitTime: float.
        """
        self.SetFocus()
        SendKey(key, waitTime)

    def SendKeys(
        self,
        text: str,
        interval: float = 0.01,
        waitTime: float = DEFAULT_OPERATION_WAIT_TIME,
        charMode: bool = True,
    ) -> None:
        """
        Make control have focus first and type keys.
        `self.SetFocus` may not work for some controls, you may need to click it to make it have focus.
        text: str, keys to type, see the docstring of `SendKeys`.
        interval: float, seconds between keys.
        waitTime: float.
        charMode: bool, if False, the text typied is depend on the input method if a input method is on.
        """
        self.SetFocus()
        SendKeys(text, interval, waitTime, charMode)

    def GetPixelColor(self, x: int, y: int) -> int:
        """
        Call native `GetPixelColor` if control has a valid native handle.
        Use `self.ToBitmap` if control doesn't have a valid native handle or you get many pixels.
        x: int, internal x position.
        y: int, internal y position.
        Return int, a color value in bgr.
        r = bgr & 0x0000FF
        g = (bgr & 0x00FF00) >> 8
        b = (bgr & 0xFF0000) >> 16
        """
        handle = self.NativeWindowHandle
        if handle:
            return GetPixelColor(x, y, handle)

    # def ToBitmap(
    #     self, x: int = 0, y: int = 0, width: int = 0, height: int = 0
    # ) -> Bitmap:
    #     """
    #     Capture control to a `Bitmap` object.
    #     x, y: int, the point in control's internal position(from 0,0).
    #     width, height: int, image's width and height from x, y, use 0 for entire area.
    #                    If width(or height) < 0, image size will be control's width(or height) - width(or height).
    #     """
    #     return Bitmap.FromControl(self, x, y, width, height)

    def CaptureToImage(
        self, savePath: str, x: int = 0, y: int = 0, width: int = 0, height: int = 0
    ) -> bool:
        """
        Capture control to a image file.
        savePath: str, should end with .bmp, .jpg, .jpeg, .png, .gif, .tif, .tiff.
        x, y: int, the point in control's internal position(from 0,0).
        width, height: int, image's width and height from x, y, use 0 for entire area.
                       If width(or height) < 0, image size will be control's width(or height) - width(or height).
        Return bool, True if succeed otherwise False.
        """
        bitmap = Bitmap.FromControl(self, x, y, width, height)
        if bitmap:
            with bitmap:
                return bitmap.ToFile(savePath)
        return False

    def IsTopLevel(self) -> bool:
        """Determine whether current control is top level."""
        handle = self.NativeWindowHandle
        if handle:
            return GetAncestor(handle, GAFlag.Root) == handle
        return False

    def GetTopLevelControl(self) -> "Control":
        """
        Get the top level control which current control lays.
        If current control is top level, return self.
        If current control is root control, return None.
        Return `PaneControl` or `WindowControl` or None.
        """
        handle = self.NativeWindowHandle
        if handle:
            topHandle = GetAncestor(handle, GAFlag.Root)
            if topHandle:
                if topHandle == handle:
                    return self
                else:
                    return ControlFromHandle(topHandle)
            else:
                # self is root control
                pass
        else:
            control = self
            while True:
                control = control.GetParentControl()
                handle = control.NativeWindowHandle
                if handle:
                    topHandle = GetAncestor(handle, GAFlag.Root)
                    return ControlFromHandle(topHandle)

    def Control(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "Control":
        return Control(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def ButtonControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "ButtonControl":
        return ButtonControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def CalendarControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "CalendarControl":
        return CalendarControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def CheckBoxControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "CheckBoxControl":
        return CheckBoxControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def ComboBoxControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "ComboBoxControl":
        return ComboBoxControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def CustomControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "CustomControl":
        return CustomControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def DataGridControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "DataGridControl":
        return DataGridControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def DataItemControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "DataItemControl":
        return DataItemControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def DocumentControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "DocumentControl":
        return DocumentControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def EditControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "EditControl":
        return EditControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def GroupControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "GroupControl":
        return GroupControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def HeaderControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "HeaderControl":
        return HeaderControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def HeaderItemControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "HeaderItemControl":
        return HeaderItemControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def HyperlinkControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "HyperlinkControl":
        return HyperlinkControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def ImageControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "ImageControl":
        return ImageControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def ListControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "ListControl":
        return ListControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def ListItemControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "ListItemControl":
        return ListItemControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def MenuControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "MenuControl":
        return MenuControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def MenuBarControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "MenuBarControl":
        return MenuBarControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def MenuItemControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "MenuItemControl":
        return MenuItemControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def PaneControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "PaneControl":
        return PaneControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def ProgressBarControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "ProgressBarControl":
        return ProgressBarControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def RadioButtonControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "RadioButtonControl":
        return RadioButtonControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def ScrollBarControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "ScrollBarControl":
        return ScrollBarControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def SemanticZoomControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "SemanticZoomControl":
        return SemanticZoomControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def SeparatorControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "SeparatorControl":
        return SeparatorControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def SliderControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "SliderControl":
        return SliderControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def SpinnerControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "SpinnerControl":
        return SpinnerControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def SplitButtonControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "SplitButtonControl":
        return SplitButtonControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def StatusBarControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "StatusBarControl":
        return StatusBarControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def TabControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "TabControl":
        return TabControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def TabItemControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "TabItemControl":
        return TabItemControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def TableControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "TableControl":
        return TableControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def TextControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "TextControl":
        return TextControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def ThumbControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "ThumbControl":
        return ThumbControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def TitleBarControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "TitleBarControl":
        return TitleBarControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def ToolBarControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "ToolBarControl":
        return ToolBarControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def ToolTipControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "ToolTipControl":
        return ToolTipControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def TreeControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "TreeControl":
        return TreeControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def TreeItemControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "TreeItemControl":
        return TreeItemControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )

    def WindowControl(
        self,
        searchDepth=0xFFFFFFFF,
        searchInterval=DEFAULT_SEARCH_INTERVAL,
        foundIndex=1,
        element=0,
        **searchProperties,
    ) -> "WindowControl":
        return WindowControl(
            searchDepth=searchDepth,
            searchInterval=searchInterval,
            foundIndex=foundIndex,
            element=element,
            searchFromControl=self,
            **searchProperties,
        )


# def create_control_from_element(element) -> Control:
#     """
#     Create a concreate `Control` from a com type `IUIAutomationElement`.
#     element: `ctypes.POINTER(IUIAutomationElement)`.
#     Return a subclass of `Control`, an instance of the control's real type.
#     """
#     if element:
#         control = ControlFactory.create_control(element.CurrentControlType)
#         if control is None:
#             logger.warning(
#                 f"element.CurrentControlType returns {element.CurrentControlType}, invalid ControlType!"
#             )
#         return control
#
#
# def create_control_from_control(control: Control) -> Control:
#     """
#     Create a concreate `Control` from a control instance, copy it.
#     control: `Control` or its subclass.
#     Return a subclass of `Control`, an instance of the control's real type.
#     For example: if control's ControlType is EditControl, return an EditControl.
#     """
#     return create_control_from_element(control.Element)


def WalkControl(control: Control, includeTop: bool = False, maxDepth: int = 0xFFFFFFFF):
    """
    control: `Control` or its subclass.
    includeTop: bool, if True, yield (control, 0) first.
    maxDepth: int, enum depth.
    Yield 2 items tuple (control: Control, depth: int).
    """
    if includeTop:
        yield control, 0
    if maxDepth <= 0:
        return
    depth = 0
    child = control.get_first_child_control()
    controlList = [child]
    while depth >= 0:
        lastControl = controlList[-1]
        if lastControl:
            yield lastControl, depth + 1
            child = lastControl.get_next_sibling_control()
            controlList[depth] = child
            if depth + 1 < maxDepth:
                child = lastControl.get_first_child_control()
                if child:
                    depth += 1
                    controlList.append(child)
        else:
            del controlList[depth]
            depth -= 1
