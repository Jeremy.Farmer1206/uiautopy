import logging
from typing import Callable, Self

import comtypes.client

from Control import Control
from control_factory import ControlFactory
from stubs.iui_stubs import IUIAutomation

logger = logging.getLogger(__name__)


class AutomationClient:
    _instance = None

    @classmethod
    def instance(cls) -> Self:
        """Singleton instance (this prevents com creation on import)."""
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance

    def __init__(self):
        self.control_factory = ControlFactory()
        tryCount = 3
        for retry in range(tryCount):
            try:
                self.UIAutomationCore = comtypes.client.GetModule(
                    "UIAutomationCore.dll"
                )
                self.IUIAutomation: IUIAutomation = comtypes.client.CreateObject(
                    "{ff48dba4-60ef-4201-aa87-54103eef594e}",
                    interface=self.UIAutomationCore.IUIAutomation,
                )
                self.ViewWalker = self.IUIAutomation.RawViewWalker
                # self.ViewWalker = self.IUIAutomation.ControlViewWalker
                break
            except Exception as ex:
                if retry + 1 == tryCount:
                    logger.exception("Could not load UIAutomationCore.dll.")
                    raise ex

    def get_root_control(self) -> Control:
        root = self.IUIAutomation.GetRootElement()
        root = self.IUIAutomation.GetRootElement()
        return self.control_factory.create_control_from_element(
            self.IUIAutomation.GetRootElement()
        )

    def find_control(
        self,
        control: Control,
        compare: Callable[[Control, int], bool],
        maxDepth: int = 0xFFFFFFFF,
        findFromSelf: bool = False,
        foundIndex: int = 1,
    ) -> Control:
        """
        control: `Control` or its subclass.
        compare: Callable[[Control, int], bool], function(control: Control, depth: int) -> bool.
        maxDepth: int, enum depth.
        findFromSelf: bool, if False, do not compare self.
        foundIndex: int, starts with 1, >= 1.
        Return `Control` subclass or None if not find.
        """
        foundCount = 0
        if not control:
            control = self.get_root_control()
        traverseCount = 0
        for child, depth in self.walk_control(control, findFromSelf, maxDepth):
            traverseCount += 1
            if compare(child, depth):
                foundCount += 1
                if foundCount == foundIndex:
                    child.traverseCount = traverseCount
                    return child

    def walk_control(
        self, control: Control, includeTop: bool = False, maxDepth: int = 0xFFFFFFFF
    ):
        """
        control: `Control` or its subclass.
        includeTop: bool, if True, yield (control, 0) first.
        maxDepth: int, enum depth.
        Yield 2 items tuple (control: Control, depth: int).
        """
        if includeTop:
            yield control, 0
        if maxDepth <= 0:
            return
        depth = 0
        child = control.get_first_child_control()
        controlList = [child]
        while depth >= 0:
            lastControl = controlList[-1]
            if lastControl:
                yield lastControl, depth + 1
                child = lastControl.get_next_sibling_control()
                controlList[depth] = child
                if depth + 1 < maxDepth:
                    child = lastControl.get_first_child_control()
                    if child:
                        depth += 1
                        controlList.append(child)
            else:
                del controlList[depth]
                depth -= 1
