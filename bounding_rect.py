from dataclasses import dataclass


@dataclass
class BoundingRect:
    left: int
    top: int
    right: int
    bottom: int

    def width(self) -> int:
        return self.right - self.left

    def height(self) -> int:
        return self.bottom - self.top

    def x_center(self) -> float:
        return (self.left + self.right) / 2

    def y_center(self) -> float:
        return (self.top + self.bottom) / 2
