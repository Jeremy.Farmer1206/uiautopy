from dataclasses import dataclass
from typing import Optional


@dataclass
class PyHResult:
    success: bool
    hresult_code: int
    error_message: Optional[str] = None
