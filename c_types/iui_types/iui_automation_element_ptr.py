from typing import Optional, Type
from ctypes import pointer
from comtypes import IUnknown
from comtypes.client import GetModule

# Import the IUIAutomationElement interface from comtypes
UIAutomationCore = GetModule("UIAutomationCore.dll")
IUIAutomationElement = UIAutomationCore.IUIAutomationElement


# Define a custom type hint for the POINTER(IUIAutomationElement) object
def IUIAutomationElementPtr() -> Type[IUIAutomationElement]:
    pass  # pragma: no cover
