from typing import List

from patterns.constants import DEFAULT_OPERATION_WAIT_TIME


class LegacyIAccessiblePattern:
    def __init__(self, pattern=None):
        """Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nn-uiautomationclient-iuiautomationlegacyiaccessiblepattern"""
        self.pattern = pattern

    @property
    def ChildId(self) -> int:
        """
        Property ChildId.
        Call IUIAutomationLegacyIAccessiblePattern::get_CurrentChildId.
        Return int, the Microsoft Active Accessibility child identifier for the element.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationlegacyiaccessiblepattern-get_currentchildid
        """
        return self.pattern.CurrentChildId

    @property
    def DefaultAction(self) -> str:
        """
        Property DefaultAction.
        Call IUIAutomationLegacyIAccessiblePattern::get_CurrentDefaultAction.
        Return str, the Microsoft Active Accessibility current default action for the element.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationlegacyiaccessiblepattern-get_currentdefaultaction
        """
        return self.pattern.CurrentDefaultAction

    @property
    def Description(self) -> str:
        """
        Property Description.
        Call IUIAutomationLegacyIAccessiblePattern::get_CurrentDescription.
        Return str, the Microsoft Active Accessibility description of the element.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationlegacyiaccessiblepattern-get_currentdescription
        """
        return self.pattern.CurrentDescription

    @property
    def Help(self) -> str:
        """
        Property Help.
        Call IUIAutomationLegacyIAccessiblePattern::get_CurrentHelp.
        Return str, the Microsoft Active Accessibility help string for the element.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationlegacyiaccessiblepattern-get_currenthelp
        """
        return self.pattern.CurrentHelp

    @property
    def KeyboardShortcut(self) -> str:
        """
        Property KeyboardShortcut.
        Call IUIAutomationLegacyIAccessiblePattern::get_CurrentKeyboardShortcut.
        Return str, the Microsoft Active Accessibility keyboard shortcut property for the element.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationlegacyiaccessiblepattern-get_currentkeyboardshortcut
        """
        return self.pattern.CurrentKeyboardShortcut

    @property
    def Name(self) -> str:
        """
        Property Name.
        Call IUIAutomationLegacyIAccessiblePattern::get_CurrentName.
        Return str, the Microsoft Active Accessibility name property of the element.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationlegacyiaccessiblepattern-get_currentname
        """
        return self.pattern.CurrentName or ""  # CurrentName may be None

    @property
    def Role(self) -> int:
        """
        Property Role.
        Call IUIAutomationLegacyIAccessiblePattern::get_CurrentRole.
        Return int, a value in calss `AccessibleRole`, the Microsoft Active Accessibility role identifier.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationlegacyiaccessiblepattern-get_currentrole
        """
        return self.pattern.CurrentRole

    @property
    def State(self) -> int:
        """
        Property State.
        Call IUIAutomationLegacyIAccessiblePattern::get_CurrentState.
        Return int, a value in calss `AccessibleState`, the Microsoft Active Accessibility state identifier.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationlegacyiaccessiblepattern-get_currentstate
        """
        return self.pattern.CurrentState

    @property
    def Value(self) -> str:
        """
        Property Value.
        Call IUIAutomationLegacyIAccessiblePattern::get_CurrentValue.
        Return str, the Microsoft Active Accessibility value property.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationlegacyiaccessiblepattern-get_currentvalue
        """
        return self.pattern.CurrentValue

    def DoDefaultAction(self, waitTime: float = DEFAULT_OPERATION_WAIT_TIME) -> bool:
        """
        Call IUIAutomationLegacyIAccessiblePattern::DoDefaultAction.
        Perform the Microsoft Active Accessibility default action for the element.
        waitTime: float.
        Return bool, True if succeed otherwise False.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationlegacyiaccessiblepattern-dodefaultaction
        """
        ret = self.pattern.DoDefaultAction() == S_OK
        time.sleep(waitTime)
        return ret

    def GetSelection(self) -> List["Control"]:
        """
        Call IUIAutomationLegacyIAccessiblePattern::GetCurrentSelection.
        Return List[Control], a list of `Control` subclasses,
                     the Microsoft Active Accessibility property that identifies the selected children of this element.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationlegacyiaccessiblepattern-getcurrentselection
        """
        eleArray = self.pattern.GetCurrentSelection()
        if eleArray:
            controls = []
            for i in range(eleArray.Length):
                ele = eleArray.GetElement(i)
                con = Control.CreateControlFromElement(element=ele)
                if con:
                    controls.append(con)
            return controls
        return []

    def GetIAccessible(self):
        """
        Call IUIAutomationLegacyIAccessiblePattern::GetIAccessible, todo.
        Return an IAccessible object that corresponds to the Microsoft UI Automation element.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationlegacyiaccessiblepattern-getiaccessible
        Refer https://docs.microsoft.com/en-us/windows/win32/api/oleacc/nn-oleacc-iaccessible
        """
        return self.pattern.GetIAccessible()

    def Select(
        self, flagsSelect: int, waitTime: float = DEFAULT_OPERATION_WAIT_TIME
    ) -> bool:
        """
        Call IUIAutomationLegacyIAccessiblePattern::Select.
        Perform a Microsoft Active Accessibility selection.
        flagsSelect: int, a value in `AccessibleSelection`.
        waitTime: float.
        Return bool, True if succeed otherwise False.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationlegacyiaccessiblepattern-select
        """
        ret = self.pattern.Select(flagsSelect) == S_OK
        time.sleep(waitTime)
        return ret

    def SetValue(
        self, value: str, waitTime: float = DEFAULT_OPERATION_WAIT_TIME
    ) -> bool:
        """
        Call IUIAutomationLegacyIAccessiblePattern::SetValue.
        Set the Microsoft Active Accessibility value property for the element.
        value: str.
        waitTime: float.
        Return bool, True if succeed otherwise False.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationlegacyiaccessiblepattern-setvalue
        """
        ret = self.pattern.set_value(value) == S_OK
        time.sleep(waitTime)
        return ret
