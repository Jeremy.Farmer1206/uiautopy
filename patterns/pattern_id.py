from enum import Enum


class PatternId(Enum):
    """
    PatternId from IUIAutomation.
    Refer https://docs.microsoft.com/en-us/windows/win32/winauto/uiauto-controlpattern-ids
    """

    AnnotationPattern = 10023
    CustomNavigationPattern = 10033
    DockPattern = 10011
    DragPattern = 10030
    DropTargetPattern = 10031
    ExpandCollapsePattern = 10005
    GridItemPattern = 10007
    GridPattern = 10006
    InvokePattern = 10000
    ItemContainerPattern = 10019
    LegacyIAccessiblePattern = 10018
    MultipleViewPattern = 10008
    ObjectModelPattern = 10022
    RangeValuePattern = 10003
    ScrollItemPattern = 10017
    ScrollPattern = 10004
    SelectionItemPattern = 10010
    SelectionPattern = 10001
    SpreadsheetItemPattern = 10027
    SpreadsheetPattern = 10026
    StylesPattern = 10025
    SynchronizedInputPattern = 10021
    TableItemPattern = 10013
    TablePattern = 10012
    TextChildPattern = 10029
    TextEditPattern = 10032
    TextPattern = 10014
    TextPattern2 = 10024
    TogglePattern = 10015
    TransformPattern = 10016
    TransformPattern2 = 10028
    ValuePattern = 10002
    VirtualizedItemPattern = 10020
    WindowPattern = 10009
