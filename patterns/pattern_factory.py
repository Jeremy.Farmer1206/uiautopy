from typing import Optional, Union

from patterns.pattern_id import PatternId
from patterns.value_pattern import ValuePattern
from patterns.legacy_iaccessible_pattern import LegacyIAccessiblePattern

_PatternIdInterfaces = None


class PatternFactory:
    def create_pattern(
        self, pattern_id: PatternId, pattern
    ) -> Optional[Union[ValuePattern, LegacyIAccessiblePattern]]:
        unmanaged_pattern = pattern.QueryInterface(
            self.GetPatternIdInterface(pattern_id)
        )
        if unmanaged_pattern:
            pattern_id = PatternId(pattern_id)
            if pattern_id == PatternId.ValuePattern:
                return ValuePattern(pattern=unmanaged_pattern)
            if pattern_id == PatternId.LegacyIAccessiblePattern:
                return LegacyIAccessiblePattern(pattern=unmanaged_pattern)
        #     return PatternConstructors[patternId](pattern=subPattern)
        #
        # if pattern_id == PatternId.ValuePattern:
        #     return ValuePattern(pattern=pattern)

    def GetPatternIdInterface(self, patternId: PatternId):
        """
        Get pattern COM interface by pattern id.
        patternId: int, a value in class `PatternId`.
        Return comtypes._cominterface_meta.
        """
        from dependencies import get_automation_client

        client = get_automation_client()
        global _PatternIdInterfaces
        if not _PatternIdInterfaces:
            _PatternIdInterfaces = {
                # PatternId.AnnotationPattern: _AutomationClient.instance().UIAutomationCore.IUIAutomationAnnotationPattern,
                # PatternId.CustomNavigationPattern: _AutomationClient.instance().UIAutomationCore.IUIAutomationCustomNavigationPattern,
                PatternId.DockPattern: client.instance().UIAutomationCore.IUIAutomationDockPattern,
                # PatternId.DragPattern: client.instance().UIAutomationCore.IUIAutomationDragPattern,
                # PatternId.DropTargetPattern: client.instance().UIAutomationCore.IUIAutomationDropTargetPattern,
                PatternId.ExpandCollapsePattern: client.instance().UIAutomationCore.IUIAutomationExpandCollapsePattern,
                PatternId.GridItemPattern: client.instance().UIAutomationCore.IUIAutomationGridItemPattern,
                PatternId.GridPattern: client.instance().UIAutomationCore.IUIAutomationGridPattern,
                PatternId.InvokePattern: client.instance().UIAutomationCore.IUIAutomationInvokePattern,
                PatternId.ItemContainerPattern: client.instance().UIAutomationCore.IUIAutomationItemContainerPattern,
                PatternId.LegacyIAccessiblePattern: client.instance().UIAutomationCore.IUIAutomationLegacyIAccessiblePattern,
                PatternId.MultipleViewPattern: client.instance().UIAutomationCore.IUIAutomationMultipleViewPattern,
                # PatternId.ObjectModelPattern: client.instance().UIAutomationCore.IUIAutomationObjectModelPattern,
                PatternId.RangeValuePattern: client.instance().UIAutomationCore.IUIAutomationRangeValuePattern,
                PatternId.ScrollItemPattern: client.instance().UIAutomationCore.IUIAutomationScrollItemPattern,
                PatternId.ScrollPattern: client.instance().UIAutomationCore.IUIAutomationScrollPattern,
                PatternId.SelectionItemPattern: client.instance().UIAutomationCore.IUIAutomationSelectionItemPattern,
                PatternId.SelectionPattern: client.instance().UIAutomationCore.IUIAutomationSelectionPattern,
                # PatternId.SpreadsheetItemPattern: client.instance().UIAutomationCore.IUIAutomationSpreadsheetItemPattern,
                # PatternId.SpreadsheetPattern: client.instance().UIAutomationCore.IUIAutomationSpreadsheetPattern,
                # PatternId.StylesPattern: client.instance().UIAutomationCore.IUIAutomationStylesPattern,
                PatternId.SynchronizedInputPattern: client.instance().UIAutomationCore.IUIAutomationSynchronizedInputPattern,
                PatternId.TableItemPattern: client.instance().UIAutomationCore.IUIAutomationTableItemPattern,
                PatternId.TablePattern: client.instance().UIAutomationCore.IUIAutomationTablePattern,
                # PatternId.TextChildPattern: client.instance().UIAutomationCore.IUIAutomationTextChildPattern,
                # PatternId.TextEditPattern: client.instance().UIAutomationCore.IUIAutomationTextEditPattern,
                PatternId.TextPattern: client.instance().UIAutomationCore.IUIAutomationTextPattern,
                # PatternId.TextPattern2: client.instance().UIAutomationCore.IUIAutomationTextPattern2,
                PatternId.TogglePattern: client.instance().UIAutomationCore.IUIAutomationTogglePattern,
                PatternId.TransformPattern: client.instance().UIAutomationCore.IUIAutomationTransformPattern,
                # PatternId.TransformPattern2: client.instance().UIAutomationCore.IUIAutomationTransformPattern2,
                PatternId.ValuePattern: client.instance().UIAutomationCore.IUIAutomationValuePattern,
                PatternId.VirtualizedItemPattern: client.instance().UIAutomationCore.IUIAutomationVirtualizedItemPattern,
                PatternId.WindowPattern: client.instance().UIAutomationCore.IUIAutomationWindowPattern,
            }
            debug = False
            # the following patterns doesn't exist on Windows 7 or lower
            try:
                _PatternIdInterfaces[
                    PatternId.AnnotationPattern
                ] = client.instance().UIAutomationCore.IUIAutomationAnnotationPattern
            except:
                if debug:
                    Logger.WriteLine(
                        "UIAutomationCore does not have AnnotationPattern.",
                        ConsoleColor.Yellow,
                    )
            try:
                _PatternIdInterfaces[
                    PatternId.CustomNavigationPattern
                ] = (
                    client.instance().UIAutomationCore.IUIAutomationCustomNavigationPattern
                )
            except:
                if debug:
                    Logger.WriteLine(
                        "UIAutomationCore does not have CustomNavigationPattern.",
                        ConsoleColor.Yellow,
                    )
            try:
                _PatternIdInterfaces[
                    PatternId.DragPattern
                ] = client.instance().UIAutomationCore.IUIAutomationDragPattern
            except:
                if debug:
                    Logger.WriteLine(
                        "UIAutomationCore does not have DragPattern.",
                        ConsoleColor.Yellow,
                    )
            try:
                _PatternIdInterfaces[
                    PatternId.DropTargetPattern
                ] = client.instance().UIAutomationCore.IUIAutomationDropTargetPattern
            except:
                if debug:
                    Logger.WriteLine(
                        "UIAutomationCore does not have DropTargetPattern.",
                        ConsoleColor.Yellow,
                    )
            try:
                _PatternIdInterfaces[
                    PatternId.ObjectModelPattern
                ] = client.instance().UIAutomationCore.IUIAutomationObjectModelPattern
            except:
                if debug:
                    Logger.WriteLine(
                        "UIAutomationCore does not have ObjectModelPattern.",
                        ConsoleColor.Yellow,
                    )
            try:
                _PatternIdInterfaces[
                    PatternId.SpreadsheetItemPattern
                ] = (
                    client.instance().UIAutomationCore.IUIAutomationSpreadsheetItemPattern
                )
            except:
                if debug:
                    Logger.WriteLine(
                        "UIAutomationCore does not have SpreadsheetItemPattern.",
                        ConsoleColor.Yellow,
                    )
            try:
                _PatternIdInterfaces[
                    PatternId.SpreadsheetPattern
                ] = client.instance().UIAutomationCore.IUIAutomationSpreadsheetPattern
            except:
                if debug:
                    Logger.WriteLine(
                        "UIAutomationCore does not have SpreadsheetPattern.",
                        ConsoleColor.Yellow,
                    )
            try:
                _PatternIdInterfaces[
                    PatternId.StylesPattern
                ] = client.instance().UIAutomationCore.IUIAutomationStylesPattern
            except:
                if debug:
                    Logger.WriteLine(
                        "UIAutomationCore does not have StylesPattern.",
                        ConsoleColor.Yellow,
                    )
            try:
                _PatternIdInterfaces[
                    PatternId.TextChildPattern
                ] = client.instance().UIAutomationCore.IUIAutomationTextChildPattern
            except:
                if debug:
                    Logger.WriteLine(
                        "UIAutomationCore does not have TextChildPattern.",
                        ConsoleColor.Yellow,
                    )
            try:
                _PatternIdInterfaces[
                    PatternId.TextEditPattern
                ] = client.instance().UIAutomationCore.IUIAutomationTextEditPattern
            except:
                if debug:
                    Logger.WriteLine(
                        "UIAutomationCore does not have TextEditPattern.",
                        ConsoleColor.Yellow,
                    )
            try:
                _PatternIdInterfaces[
                    PatternId.TextPattern2
                ] = client.instance().UIAutomationCore.IUIAutomationTextPattern2
            except:
                if debug:
                    Logger.WriteLine(
                        "UIAutomationCore does not have TextPattern2.",
                        ConsoleColor.Yellow,
                    )
            try:
                _PatternIdInterfaces[
                    PatternId.TransformPattern2
                ] = client.instance().UIAutomationCore.IUIAutomationTransformPattern2
            except:
                if debug:
                    Logger.WriteLine(
                        "UIAutomationCore does not have TransformPattern2.",
                        ConsoleColor.Yellow,
                    )
        return _PatternIdInterfaces[patternId]
