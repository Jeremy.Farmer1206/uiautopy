from py_hresult import PyHResult
from patterns.constants import DEFAULT_OPERATION_WAIT_TIME, S_OK
import time
import ctypes
import logging

logger = logging.getLogger(__name__)


def get_error_message(hr):
    FORMAT_MESSAGE_FROM_SYSTEM = 0x00001000
    buf = ctypes.create_unicode_buffer(1024)
    msglen = ctypes.windll.kernel32.FormatMessageW(
        FORMAT_MESSAGE_FROM_SYSTEM, None, hr, 0, buf, len(buf), None
    )
    if msglen == 0:
        return "Unknown error"
    else:
        return buf.value.strip()


class ValuePattern:
    def __init__(self, pattern=None):
        """Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nn-uiautomationclient-iuiautomationvaluepattern"""
        self.pattern = pattern

    @property
    def is_read_only(self) -> bool:
        """
        Property IsReadOnly.
        Call IUIAutomationTransformPattern2::IUIAutomationValuePattern::get_CurrentIsReadOnly.
        Return bool, indicates whether the value of the element is read-only.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationvaluepattern-get_currentisreadonly
        """
        return bool(self.pattern.CurrentIsReadOnly)

    @property
    def value(self) -> str:
        """
        Property Value.
        Call IUIAutomationTransformPattern2::IUIAutomationValuePattern::get_CurrentValue.
        Return str, the value of the element.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationvaluepattern-get_currentvalue
        """
        return self.pattern.CurrentValue

    def set_value(
        self, value: str, wait_time: float = DEFAULT_OPERATION_WAIT_TIME
    ) -> PyHResult:
        """
        Call IUIAutomationTransformPattern2::IUIAutomationValuePattern::SetValue.
        Set the value of the element.
        value: str.
        waitTime: float.
        Return bool, True if succeed otherwise False.
        Refer https://docs.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationvaluepattern-setvalue
        """
        hresult = self.pattern.SetValue(value)  # == S_OK
        if hresult != S_OK:
            error_message = get_error_message(hresult)
            logger.warning(f"SetValue failed with HRESULT {hresult} : {error_message}")
            return PyHResult(
                success=False, error_message=error_message, hresult_code=hresult
            )
        time.sleep(wait_time)
        return PyHResult(success=True, hresult_code=hresult)
