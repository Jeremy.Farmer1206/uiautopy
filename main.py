from Control import Control
from automation_client import AutomationClient
from control_type import ControlTypeId

from controls.window_control import WindowControl
from patterns.pattern_id import PatternId
from searching.search_properties import SearchProperties
from control_factory import ControlFactory
from dependencies import get_automation_client
import logging


def get_notepad(control: Control) -> bool:
    return control.name == "*Untitled - Notepad"


def main():
    logging.basicConfig(level=logging.DEBUG)

    client = get_automation_client()
    desktop = client.get_root_control()
    first_child = desktop.get_first_child_control()
    sibling = first_child.get_sibling_control(get_notepad, forward=False)
    print(sibling)
    doc = sibling.get_first_child_control()
    handle = doc.native_window_handle
    rect = doc.bounding_rectangle
    print(rect)
    # print(f"Notepad Text:  {doc.GetPattern(PatternId.ValuePattern).Value}")
    print(f"Notepad Text:  {doc.value_pattern.value}")
    print(
        f"Notepad Text IsReadOnly:  {doc.get_pattern(PatternId.ValuePattern).is_read_only}"
    )
    print(f"Notepad Text IsPassword:  {doc.is_password}")

    print(f"Notepad Text Clicky Point:  {doc.get_clickable_point()}")
    print(doc.control_type_name)
    # print(
    #     f"Accelerator {first_child.GetPattern(PatternId.LegacyIAccessiblePattern).Name}"
    # )


if __name__ == "__main__":
    main()
