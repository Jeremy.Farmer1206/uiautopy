import pytest
from Control import Control
from unittest.mock import Mock, MagicMock
import comtypes

from controls.app_bar_control import AppBarControl
from controls.edit_control import EditControl
from patterns.pattern_id import PatternId
from patterns.value_pattern import ValuePattern


@pytest.fixture
def mock_app_bar_control():
    app_bar = AppBarControl()
    mock_element = MagicMock()

    mock_unmanage_value_pattern = MagicMock()
    mock_unmanage_value_pattern.CurrentValue = "asdf"
    mock_unmanage_value_pattern.CurrentIsReadOnly = 0
    mock_unmanage_value_pattern.SetValue.return_value = 0

    pattern_interface = MagicMock()
    pattern_interface.QueryInterface.return_value = mock_unmanage_value_pattern
    # pattern_interface
    mock_element.GetCurrentPattern.return_value = pattern_interface

    app_bar.Element = mock_element
    yield app_bar


class TestAppBarControl:
    def test_app_bar_control_type_name(self, mock_app_bar_control):
        assert mock_app_bar_control.control_type_name == "AppBarControl"
