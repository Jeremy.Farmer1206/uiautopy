import pytest
from Control import Control
from unittest.mock import Mock, MagicMock
import comtypes
from controls.edit_control import EditControl
from controls.pane_control import PaneControl
from patterns.pattern_id import PatternId
from patterns.value_pattern import ValuePattern


# @pytest.fixture
# def mock_edit_control():
#     mock_element = MagicMock()
#     mock_element.GetCurrentPattern.return_value = MagicMock()
#     edit_control = EditControl(element=mock_element)
#     yield edit_control
#
#
@pytest.fixture
def mock_pane_control():
    edit = PaneControl()
    mock_element = MagicMock()

    mock_unmanage_value_pattern = MagicMock()
    mock_unmanage_value_pattern.CurrentValue = "asdf"
    mock_unmanage_value_pattern.CurrentIsReadOnly = 0
    mock_unmanage_value_pattern.SetValue.return_value = 0

    pattern_interface = MagicMock()
    pattern_interface.QueryInterface.return_value = mock_unmanage_value_pattern
    # pattern_interface
    mock_element.GetCurrentPattern.return_value = pattern_interface

    edit.Element = mock_element
    yield edit


class TestEditControl:
    def test_edit_control_type_name(self, mock_pane_control: PaneControl):
        assert mock_pane_control.control_type_name == "PaneControl"
