import pytest
from Control import Control
from unittest.mock import Mock, MagicMock
import comtypes
from controls.edit_control import EditControl
from patterns.pattern_id import PatternId
from patterns.value_pattern import ValuePattern


@pytest.fixture
def mock_edit_control(mock_raw_edit_control, mock_unmanage_value_pattern):
    edit = EditControl()

    pattern_interface = MagicMock()
    pattern_interface.QueryInterface.return_value = mock_unmanage_value_pattern
    mock_raw_edit_control.GetCurrentPattern.return_value = pattern_interface

    edit.Element = mock_raw_edit_control
    yield edit


@pytest.fixture
def mock_raw_edit_control():
    mock_element = MagicMock()
    return mock_element


@pytest.fixture
def mock_unmanage_value_pattern():
    pattern = MagicMock()
    pattern.CurrentValue = "asdf"
    pattern.CurrentIsReadOnly = 0
    pattern.CurrentIsPassword = 1
    pattern.SetValue.return_value = 0
    return pattern


@pytest.fixture
def mock_raw_point():
    point = MagicMock()
    point.x = 0
    point.y = 0
    return point


class TestEditControl:
    def test_edit_control_type_name(self, mock_edit_control):
        assert mock_edit_control.control_type_name == "EditControl"

    def test_edit_control_value_pattern(
        self, mock_edit_control, mock_unmanage_value_pattern
    ):
        mock_unmanage_value_pattern.CurrentValue = "new_value"
        assert mock_edit_control.value_pattern.value == "new_value"

    def test_edit_control_clickable_point(
        self, mock_edit_control, mock_raw_edit_control, mock_raw_point
    ):
        mock_raw_point.x = 0
        mock_raw_point.y = 1
        mock_raw_edit_control.GetClickablePoint.return_value = (mock_raw_point, 1)
        clickable_point = mock_edit_control.get_clickable_point()
        assert clickable_point.x == 0
        assert clickable_point.y == 1
        assert clickable_point.got_clickable is True

    def test_edit_control_clickable_point_unclickable(
        self, mock_edit_control, mock_raw_edit_control, mock_raw_point
    ):
        mock_raw_point.x = 0
        mock_raw_point.y = 0
        mock_raw_edit_control.GetClickablePoint.return_value = (mock_raw_point, 0)

        clickable_point = mock_edit_control.get_clickable_point()

        assert clickable_point.x == 0
        assert clickable_point.y == 0
        assert clickable_point.got_clickable is False

    def test_edit_control_is_password_true(
        self, mock_edit_control, mock_raw_edit_control
    ):
        # Note(jf6856): https://learn.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentispassword
        # returns either a 0 or 1
        mock_raw_edit_control.CurrentIsPassword = 1
        assert mock_edit_control.is_password is True

    def test_edit_control_is_password_false(
        self, mock_edit_control, mock_raw_edit_control
    ):
        # Note(jf6856): https://learn.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationelement-get_currentispassword
        # returns either a 0 or 1
        mock_raw_edit_control.CurrentIsPassword = 0
        assert mock_edit_control.is_password is False
