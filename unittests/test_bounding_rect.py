import pytest
from Control import Control
from unittest.mock import Mock, MagicMock
import comtypes

from bounding_rect import BoundingRect
from controls.app_bar_control import AppBarControl
from controls.edit_control import EditControl
from patterns.pattern_id import PatternId
from patterns.value_pattern import ValuePattern


@pytest.fixture
def bounding_rect():
    rect = BoundingRect(left=0, right=10, top=0, bottom=10)
    return rect


class TestBoundingRect:
    def test_width(self, bounding_rect):
        assert bounding_rect.width() == 10

    def test_height(self, bounding_rect):
        assert bounding_rect.height() == 10

    def test_x_center(self, bounding_rect):
        assert bounding_rect.x_center() == 5

    def test_y_center(self, bounding_rect):
        assert bounding_rect.y_center() == 5

    def test_eq_are_eq(self, bounding_rect):
        other_rect = BoundingRect(left=0, right=10, top=0, bottom=10)
        assert bounding_rect == other_rect

    def test_eq_not_eq(self, bounding_rect):
        other_rect = BoundingRect(left=0, right=10, top=0, bottom=11)
        assert bounding_rect != other_rect
