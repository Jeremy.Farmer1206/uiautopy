import pytest
from Control import Control
from unittest.mock import Mock, MagicMock
import comtypes
from controls.edit_control import EditControl
from patterns.pattern_id import PatternId
from patterns.value_pattern import ValuePattern


@pytest.fixture
def mock_edit_control(mock_raw_edit_control, mock_unmanage_value_pattern):
    edit = EditControl()

    pattern_interface = MagicMock()
    pattern_interface.QueryInterface.return_value = mock_unmanage_value_pattern
    mock_raw_edit_control.GetCurrentPattern.return_value = pattern_interface
    mock_raw_edit_control.CurrentName = "Text Editor"
    mock_raw_edit_control.CurrentNativeWindowHandle = 51436
    mock_raw_edit_control.CurrentBoundingRectangle.left = 0
    mock_raw_edit_control.CurrentBoundingRectangle.right = 10
    mock_raw_edit_control.CurrentBoundingRectangle.top = 0
    mock_raw_edit_control.CurrentBoundingRectangle.bottom = 0

    edit.Element = mock_raw_edit_control
    yield edit


@pytest.fixture
def mock_raw_edit_control():
    mock_element = MagicMock()
    return mock_element


@pytest.fixture
def mock_unmanage_value_pattern():
    pattern = MagicMock()
    pattern.CurrentValue = "asdf"
    pattern.CurrentIsReadOnly = 0
    pattern.CurrentIsPassword = 1
    pattern.SetValue.return_value = 0
    return pattern


@pytest.fixture
def mock_raw_point():
    point = MagicMock()
    point.x = 0
    point.y = 0
    return point


class TestControl:
    def test_control_dunder_str(self, mock_edit_control, mock_raw_edit_control):
        mock_raw_edit_control.CurrentClassName = "Edit"
        mock_raw_edit_control.CurrentAutomationId = 15
        str_rep = mock_edit_control.__str__()

        expected = (
            f"ControlType: EditControl    ClassName: Edit    AutomationId: 15    Rect: BoundingRect(left=0, "
            f"top=0, right=10, bottom=0)    Name: Text Editor    Handle: 51436)"
        )
        assert str_rep == expected

    def test_get_unsupported_pattern_returns_none(self, mock_edit_control):
        mock_edit_control.Element.GetCurrentPattern.return_value = None
        result = mock_edit_control.get_pattern(PatternId.ValuePattern)
        other_result = mock_edit_control.value_pattern
        assert result is None
        assert other_result is None

    def test_get_value_pattern_returns_value_pattern(self, mock_edit_control):
        value_pattern = mock_edit_control.get_pattern(PatternId.ValuePattern)

        assert isinstance(value_pattern, ValuePattern)
        assert value_pattern.value == "asdf"

    def test_get_value_pattern_get_value(self, mock_edit_control):
        assert mock_edit_control.value_pattern.value == "asdf"

    def test_get_value_pattern_is_read_only(self, mock_edit_control):
        assert mock_edit_control.value_pattern.is_read_only is False

    def test_value_pattern_set_value(self, mock_edit_control):
        hres = mock_edit_control.value_pattern.set_value("test")
        assert hres.success is True

    def test_value_pattern_set_value_hresult_failure(self, mock_edit_control):
        mock_edit_control.value_pattern.pattern.SetValue.return_value = -2147024891
        hres = mock_edit_control.value_pattern.set_value("test")
        assert hres.success is False

    def test_com_error(self, mock_edit_control):
        mock_edit_control.Element.GetCurrentPattern.side_effect = comtypes.COMError(
            0, 0, 0
        )
        result = mock_edit_control.get_pattern(PatternId.ValuePattern)

        assert result is None
