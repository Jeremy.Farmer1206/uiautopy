from comtypes import IUnknown, HRESULT, GUID
from ctypes import c_int, c_void_p, POINTER


class IUIAutomationElement(IUnknown):
    _iid_ = GUID("{d22108aa-8ac5-49a5-837b-37bb91740e21}")
    _methods_ = []

    @property
    def CurrentIsPassword(self) -> int:
        pass


class IUIAutomation(IUnknown):
    _iid_ = GUID("{ff48dba4-60ef-4201-aa87-54103eef594e}")
    _methods_ = []

    def GetRootElement(self) -> IUIAutomationElement:
        pass
