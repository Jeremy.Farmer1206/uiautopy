from automation_client import AutomationClient
from functools import lru_cache
from searching.view_walker import ViewWalker
from control_factory import ControlFactory
from patterns.pattern_factory import PatternFactory


@lru_cache
def get_automation_client() -> AutomationClient:
    return AutomationClient()


@lru_cache
def get_view_walker():
    view_walker = ViewWalker(get_automation_client().ViewWalker)
    return view_walker
    # # TODO: Can we get typing information here at all, this is a RawViewWalker
    # return get_automation_client().ViewWalker


@lru_cache
def get_control_factory() -> ControlFactory:
    return ControlFactory()


@lru_cache
def get_pattern_factory() -> PatternFactory:
    return PatternFactory()
