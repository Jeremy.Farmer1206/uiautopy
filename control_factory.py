from typing import Union, Optional

from control_type import ControlTypeId
from controls.edit_control import EditControl
from controls.pane_control import PaneControl
from controls.window_control import WindowControl
import logging

from stubs.iui_stubs import IUIAutomationElement

logger = logging.getLogger(__name__)


class ControlFactory:
    from controls.app_bar_control import AppBarControl

    def create_control(
        self, control_type: int, element: IUIAutomationElement
    ) -> Optional[Union[AppBarControl, WindowControl, PaneControl, EditControl]]:
        from controls.app_bar_control import AppBarControl

        control_type_id = ControlTypeId(control_type)

        if control_type_id == ControlTypeId.AppBarControl:
            return AppBarControl(element=element)
        elif control_type_id == ControlTypeId.WindowControl:
            return WindowControl(element=element)
        elif control_type_id == ControlTypeId.PaneControl:
            return PaneControl(element=element)
        elif control_type_id == ControlTypeId.EditControl:
            return EditControl(element=element)

        logger.warning(
            f"Control Type Id {control_type} did not match any defined options"
        )
        return None

    def create_control_from_element(
        self,
        element: IUIAutomationElement,
    ) -> Optional[Union[AppBarControl, WindowControl]]:
        """
        Create a concreate `Control` from a com type `IUIAutomationElement`.
        element: `ctypes.POINTER(IUIAutomationElement)`.
        Return a subclass of `Control`, an instance of the control's real type.
        """
        if element:
            return self.create_control(element.CurrentControlType, element)
