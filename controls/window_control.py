from typing import Optional

from Control import Control
from control_type import ControlTypeId
from controls.top_level_control import TopLevelControl
from controls.constants import DEFAULT_SEARCH_INTERVAL
from searching.search_properties import SearchProperties


class WindowControl(TopLevelControl):
    @property
    def control_type_name(self) -> str:
        return "WindowControl"

    def __init__(
        self,
        search_from_control: Optional[Control] = None,
        search_depth: int = 0xFFFFFFFF,
        search_interval: float = DEFAULT_SEARCH_INTERVAL,
        found_index: int = 1,
        element=None,
        search_properties: Optional[SearchProperties] = None,
    ):
        super().__init__(
            search_from_control,
            search_depth,
            search_interval,
            found_index,
            element,
            search_properties,
        )
        if search_properties is not None:
            self.search_properties.control_type = ControlTypeId.WindowControl
        self._DockPattern = None
        self._TransformPattern = None

    # def GetTransformPattern(self) -> TransformPattern:
    #     """
    #     Return `TransformPattern` if it supports the pattern else None(Must support according to MSDN).
    #     """
    #     return self.GetPattern(PatternId.TransformPattern)
    #
    # def GetWindowPattern(self) -> WindowPattern:
    #     """
    #     Return `WindowPattern` if it supports the pattern else None(Must support according to MSDN).
    #     """
    #     return self.GetPattern(PatternId.WindowPattern)
    #
    # def GetDockPattern(self) -> DockPattern:
    #     """
    #     Return `DockPattern` if it supports the pattern else None(Conditional support according to MSDN).
    #     """
    #     return self.GetPattern(PatternId.DockPattern)
    #
    # def MetroClose(self, waitTime: float = OPERATION_WAIT_TIME) -> None:
    #     """
    #     Only work on Windows 8/8.1, if current window is Metro UI.
    #     waitTime: float.
    #     """
    #     if self.ClassName == METRO_WINDOW_CLASS_NAME:
    #         screenWidth, screenHeight = GetScreenSize()
    #         MoveTo(screenWidth // 2, 0, waitTime=0)
    #         DragDrop(
    #             screenWidth // 2, 0, screenWidth // 2, screenHeight, waitTime=waitTime
    #         )
    #     else:
    #         Logger.WriteLine("Window is not Metro!", ConsoleColor.Yellow)
