from typing import Optional

from Control import Control
from control_type import ControlTypeId
from controls.constants import DEFAULT_SEARCH_INTERVAL
from controls.top_level_control import TopLevelControl
from searching.search_properties import SearchProperties


class PaneControl(TopLevelControl):
    @property
    def control_type_name(self) -> str:
        return "PaneControl"

    def __init__(
        self,
        search_from_control: Optional[Control] = None,
        search_depth: int = 0xFFFFFFFF,
        search_interval: float = DEFAULT_SEARCH_INTERVAL,
        found_index: int = 1,
        element=None,
        search_properties: Optional[SearchProperties] = SearchProperties(),
    ):
        super().__init__(
            search_from_control,
            search_depth,
            search_interval,
            found_index,
            element,
            search_properties,
        )
        self.search_properties.control_type = ControlTypeId.PaneControl

    # def GetDockPattern(self) -> DockPattern:
    #     """
    #     Return `DockPattern` if it supports the pattern else None(Conditional support according to MSDN).
    #     """
    #     return self.GetPattern(PatternId.DockPattern)
    #
    # def GetScrollPattern(self) -> ScrollPattern:
    #     """
    #     Return `ScrollPattern` if it supports the pattern else None(Conditional support according to MSDN).
    #     """
    #     return self.GetPattern(PatternId.ScrollPattern)
    #
    # def GetTransformPattern(self) -> TransformPattern:
    #     """
    #     Return `TransformPattern` if it supports the pattern else None(Conditional support according to MSDN).
    #     """
    #     return self.GetPattern(PatternId.TransformPattern)
