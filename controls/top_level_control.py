from Control import Control
from controls.constants import DEFAULT_OPERATION_WAIT_TIME


class TopLevelControl(Control):
    """Class TopLevel"""

    # def SetTopmost(
    #     self, isTopmost: bool = True, waitTime: float = DEFAULT_OPERATION_WAIT_TIME
    # ) -> bool:
    #     """
    #     Set top level window topmost.
    #     isTopmost: bool.
    #     waitTime: float.
    #     """
    #     if self.IsTopLevel():
    #         ret = SetWindowTopmost(self.NativeWindowHandle, isTopmost)
    #         time.sleep(waitTime)
    #         return ret
    #     return False
    #
    # def IsTopmost(self) -> bool:
    #     if self.IsTopLevel():
    #         WS_EX_TOPMOST = 0x00000008
    #         return bool(
    #             GetWindowLong(self.NativeWindowHandle, GWL.ExStyle) & WS_EX_TOPMOST
    #         )
    #     return False
    #
    # def SwitchToThisWindow(self, waitTime: float = OPERATION_WAIT_TIME) -> None:
    #     if self.IsTopLevel():
    #         SwitchToThisWindow(self.NativeWindowHandle)
    #         time.sleep(waitTime)
    #
    # def Maximize(self, waitTime: float = OPERATION_WAIT_TIME) -> bool:
    #     """
    #     Set top level window maximize.
    #     """
    #     if self.IsTopLevel():
    #         return self.ShowWindow(SW.ShowMaximized, waitTime)
    #     return False
    #
    # def IsMaximize(self) -> bool:
    #     if self.IsTopLevel():
    #         return bool(IsZoomed(self.NativeWindowHandle))
    #     return False
    #
    # def Minimize(self, waitTime: float = OPERATION_WAIT_TIME) -> bool:
    #     if self.IsTopLevel():
    #         return self.ShowWindow(SW.Minimize, waitTime)
    #     return False
    #
    # def IsMinimize(self) -> bool:
    #     if self.IsTopLevel():
    #         return bool(IsIconic(self.NativeWindowHandle))
    #     return False
    #
    # def Restore(self, waitTime: float = OPERATION_WAIT_TIME) -> bool:
    #     """
    #     Restore window to normal state.
    #     Similar to SwitchToThisWindow.
    #     """
    #     if self.IsTopLevel():
    #         return self.ShowWindow(SW.Restore, waitTime)
    #     return False
    #
    # def MoveToCenter(self) -> bool:
    #     """
    #     Move window to screen center.
    #     """
    #     if self.IsTopLevel():
    #         rect = self.BoundingRectangle
    #         screenWidth, screenHeight = GetScreenSize()
    #         x, y = (screenWidth - rect.width()) // 2, (
    #             screenHeight - rect.height()
    #         ) // 2
    #         if x < 0:
    #             x = 0
    #         if y < 0:
    #             y = 0
    #         return SetWindowPos(
    #             self.NativeWindowHandle, SWP.HWND_Top, x, y, 0, 0, SWP.SWP_NoSize
    #         )
    #     return False
    #
    # def SetActive(self, waitTime: float = OPERATION_WAIT_TIME) -> bool:
    #     """Set top level window active."""
    #     if self.IsTopLevel():
    #         handle = self.NativeWindowHandle
    #         if IsIconic(handle):
    #             ret = ShowWindow(handle, SW.Restore)
    #         elif not IsWindowVisible(handle):
    #             ret = ShowWindow(handle, SW.Show)
    #         ret = SetForegroundWindow(
    #             handle
    #         )  # may fail if foreground windows's process is not python
    #         time.sleep(waitTime)
    #         return ret
    #     return False
