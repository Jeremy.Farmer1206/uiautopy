from dataclasses import dataclass


@dataclass
class ClickablePointProperty:
    x: int
    y: int
    got_clickable: bool
