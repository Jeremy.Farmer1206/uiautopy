from dataclasses import dataclass
from typing import Callable, Optional

from control_type import ControlTypeId


@dataclass
class SearchProperties:
    control_type: Optional[ControlTypeId] = None
    class_name: Optional[str] = None
    automation_id: Optional[str] = None
    name: Optional[str] = None
    sub_name: Optional[str] = None
    regex_name: Optional[str] = None
    depth: Optional[int] = 0xFFFFFFFF
    # compare: Optional[Callable[[Control, int], bool]] = None

    def __post_init__(self):
        if self.sub_name and self.regex_name:
            raise ValueError(
                "You can only use one of sub_name and regex_name in searchProperties."
            )
        if self.depth:
            self.search_depth = self.depth

    def __len__(self):
        return len(self.__dict__)
