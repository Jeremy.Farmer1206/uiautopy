from ctypes import POINTER
from typing import Optional
from c_types.iui_types.iui_automation_element_ptr import IUIAutomationElementPtr
from stubs.iui_stubs import IUIAutomationElement


class ViewWalker:
    def __init__(self, raw_view_walker):
        self.raw_view_walker = raw_view_walker

    def get_first_child_element(
        self, raw_element: IUIAutomationElement
    ) -> Optional[IUIAutomationElement]:
        return self.raw_view_walker.GetFirstChildElement(raw_element)

    def get_next_sibling_element(
        self, raw_element
    ) -> Optional[IUIAutomationElementPtr]:
        """
        Calls IUIAutomationTreeWalker::GetNextSiblingElement
        https://learn.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationtreewalker-getnextsiblingelement
        :param raw_element:
        :return: raw IUIAutomationElement or None if there is no sibling
        """
        return self.raw_view_walker.GetNextSiblingElement(raw_element)

    def get_previous_sibling_element(self, raw_element):
        """
        Calls IUIAutomationTreeWalker::GetNextSiblingElement
        https://learn.microsoft.com/en-us/windows/win32/api/uiautomationclient/nf-uiautomationclient-iuiautomationtreewalker-getnextsiblingelement
        :param raw_element:
        :return: raw IUIAutomationElement or None if there is no sibling
        """
        return self.raw_view_walker.GetPreviousSiblingElement(raw_element)
